package JUnit;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;




class AssertTest {

    @Test
    public void AssertEqual() {
        Assert fullName = new Assert("Adam Jacob Cross", 1024);
        assertEquals("Adam Jacob Cross", fullName.getName());

    }

    @Test
    public void AssertTrue() {
        Assert True = new Assert("Adam Cross", 1024);
        assertTrue(True.AssertTrue("Adam Cross"), True.getName());
    }

    @Test
    public void AssertFalse() {
        Assert False = new Assert("Adam Cross", 1024);
        assertFalse(False.AssertTrue("Adam Jacob Cross"), False.getName());
    }


    @Test
    public void AssertArrayEqual() {

        String[] Output1 = {"red", "yellow", "blue"};
        String[] Output2 = {"red", "yellow", "blue"};
        assertArrayEquals(Output1, Output2);
    }

    @Test
    public void AssertNotNull() {
        Assert NotNUlL = new Assert("Adam Jacob Cross", 1024);
        assertNotNull("Not Adam");

    }

    @Test
    public void AssertNull() {
        Assert NULLName = new Assert(null, 1024);
        assertNull(null);

    }

    @Test
    public void Same() {
        String A = "Adam Jacob Cross";
        String B = "Adam Jacob Cross";
        assertSame(A, B, "These names are the same");
    }

    @Test
    public void NotSame() {
        String A = "Adam Jacob Cross";
        String B = "Adam Cross";
        assertNotSame(A, B, "These names are not the same");
    }
}

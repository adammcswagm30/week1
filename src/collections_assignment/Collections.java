package collections_assignment;

import java.util.*;

public class Collections {

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please provide a valid sentence with more than one word: \n (Include two of the same words to see the effect of a Set)");
        String input = scanner.nextLine();

        String[] array = input.split(" ");
        if (array.length == 1){
            System.out.println("ERROR: Only one word was provided. Sentence is invalid.");
        }
        else {
            //      List Creation
            System.out.println("List collection:");
            List<String> list = new ArrayList();

            for (int i = 0; i < array.length; i++) {
                list.add(array[i]);
            }

            for (String word : list) {
                System.out.println(word);
            }
            //       Queue Creation
            System.out.println();
            System.out.println("Queue collection:");
            Queue<String> que = new PriorityQueue();

            for (int i = 0; i < array.length; i++) {
                que.add(array[i]);
            }

            Iterator iterator = que.iterator();
            while (iterator.hasNext()) {
                System.out.println(que.poll());
            }

            //       Set Creation
            System.out.println();
            System.out.println("Set collection:");
            Set<String> set = new TreeSet();

            for (int i = 0; i < array.length; i++) {
                set.add(array[i]);
            }

            for (String word : set) {
                System.out.println(word);
            }
            //       Map Creation
            System.out.println();
            System.out.println("Map collection:");
            Map map = new HashMap();

            for (int i = 0; i < array.length; i++) {
                map.put(i, array[i]);
            }

            for (int i = 0; i < map.size(); i++) {
                System.out.println(i + " - " + (String) map.get(i));
            }
        }
    }

}

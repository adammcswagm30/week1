package JSON;

import JSON.HTTPServer.MadLibWords;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.NameValuePair;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Scanner;

public class HTTPClient {
    static Scanner scanner = new Scanner(System.in);
    public static Person JSONToPerson(String string){
        ObjectMapper mapper = new ObjectMapper();
        Person person = null;

        try{
            person = mapper.readValue(string, Person.class);

        }
        catch (JsonProcessingException a){
            System.err.println(a.toString());
        }

        return person;
    }

    private static String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }


    public static String HttpContent(String string){

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

           //http.setReadTimeout(10000);
           //http.setConnectTimeout(15000);
           //http.setRequestMethod("POST");
           //http.setDoInput(true);
           //http.setDoOutput(true);

           //List<NameValuePair> params = new ArrayList<NameValuePair>();
           //params.add(new BasicNameValuePair("firstParam", "hello"));
           //params.add(new BasicNameValuePair("secondParam", "there"));
           //params.add(new BasicNameValuePair("thirdParam", "genera kenobi"));
           //params.add(new BasicNameValuePair("fourthParam", "genera kenobi"));
           //params.add(new BasicNameValuePair("fifthParam", "genera kenobi"));
           //params.add(new BasicNameValuePair("sixthParam", "genera kenobi"));


           //OutputStream os = http.getOutputStream();
           //BufferedWriter writer = new BufferedWriter(
           //        new OutputStreamWriter(os, "UTF-8"));
           //writer.write(getQuery(params));
           //writer.flush();
           //writer.close();
           //os.close();

           //http.connect();

        InputStream inputStream = http.getInputStream();
        BufferedReader read = new BufferedReader(new InputStreamReader(http.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();

        String line = null;
        while ((line = read.readLine()) != null){
            stringBuilder.append(line + "\n");
        }

        return stringBuilder.toString();
        }
        catch (IOException a) {
            System.err.println(a.toString());
            System.out.println("SOMETHING BROKE");
        }

        return "Error";
    }
    //public static String getNoun1(){
    //    return noun1;
    //}
    public static void playGame(){
        String noun1 = getAnimal();
        String noun2 = getNoun();
        String noun3 = getNoun();
        String adj1 = getAdjective();
        String place1 = getPlace();
        String verb1 = getVerb();

        MadLibWords madLibWords = new MadLibWords(noun1, noun2, noun3, adj1, place1, verb1);


        String madLib = HTTPClient.HttpContent("http://127.0.0.1:8080?noun1=" + noun1 + "&noun2=" + noun2 + "&noun3=" + noun3 + "&adj1=" + adj1 + "&place1=" + place1 + "&verb1=" +verb1);
        //HTTPClient hello = HTTPClient.newBuilder();
        System.out.println(madLib);
        //Person person = JSONToPerson(json);
        //System.out.println(person);
    }

    public static String getNoun(){
        System.out.println("Please Enter a Noun:\n");
        String noun = scanner.nextLine();
        return noun;
    }
    public static String getAdjective(){
        System.out.println("Please Enter an adjective:\n");
        String adjective = scanner.nextLine();
        return adjective;
    }
    public static String getVerb(){
        System.out.println("Please Enter a Verb (ending in ing):\n");
        String verb = scanner.nextLine();
        return verb;
    }
    public static String getPlace(){
        System.out.println("Please Enter the name of a place:\n");
        String place = scanner.nextLine();
        return place;
    }
    public static String getAnimal(){
        System.out.println("Please Enter the name of an animal:\n");
        String noun = scanner.nextLine();
        return noun;
    }



    public static void main(String[] args) {
        while(true) {
        System.out.println("Please type start and hit enter to begin a MadLib Game:");

        String start = scanner.nextLine();


            if (start.toLowerCase().equals("start")) {
                playGame();
                break;
            }
        }

    }
}

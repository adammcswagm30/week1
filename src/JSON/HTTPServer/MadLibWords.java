package JSON.HTTPServer;

public class MadLibWords {
    String noun1;
    String noun2;
    String noun3;
    String adj1;
    String place1;
    String verb1;

    public MadLibWords(String noun1, String noun2, String noun3, String adj1, String place1, String verb1) {
        this.noun1 = noun1;
        this.noun2 = noun2;
        this.noun3 = noun3;
        this.adj1 = adj1;
        this.place1 = place1;
        this.verb1 = verb1;
    }

    public String getNoun1() {
        return noun1;
    }

    public void setNoun1(String noun1) {
        this.noun1 = noun1;
    }

    public String getNoun2() {
        return noun2;
    }

    public void setNoun2(String noun2) {
        this.noun2 = noun2;
    }

    public String getNoun3() {
        return noun3;
    }

    public void setNoun3(String noun3) {
        this.noun3 = noun3;
    }

    public String getAdj1() {
        return adj1;
    }

    public void setAdj1(String adj1) {
        this.adj1 = adj1;
    }

    public String getPlace1() {
        return place1;
    }

    public void setPlace1(String place1) {
        this.place1 = place1;
    }

    public String getVerb1() {
        return verb1;
    }

    public void setVerb1(String verb1) {
        this.verb1 = verb1;
    }
}

package JSON.HTTPServer.core;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


public class ServerListenerThread extends Thread{

    private int port;
    private String webroot;
    private ServerSocket serverSocket;

    public ServerListenerThread(int port, String webroot) throws IOException {
        this.port = port;
        this.webroot = webroot;
        this.serverSocket = new ServerSocket(this.port);

    }

    @Override
    public void run(){


        while(true) {
            try {

                Socket socket = serverSocket.accept();
                InputStream inputStream = socket.getInputStream();
                // Adam addition 3/31
                BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String testWords = input.readLine();
                String array[] = testWords.split(" ");
                String array2[] = array[1].split("&");
                ArrayList list = new ArrayList();
                for (int j = 0; j < array2.length; j++) {
                    String array3[] = array2[j].split("=");

                    for (int i = 0; i < array3.length; i++) {
                        if (i % 2 != 0) {
                            list.add(array3[i]);
                        }
                    }
                }

                // End Adam's stuff


                String noun1 = (String) list.get(0);
                String noun2 = (String) list.get(1);
                String noun3 = (String) list.get(2);
                String adj1 = (String) list.get(3);
                String place1 = (String) list.get(4);
                String verb1 = (String) list.get(5);

                //Person person1 = new Person("Adam", 12345);
                String body = "This little " + noun1 + " went to the market,\n" +
                        "This little piggy stayed at " + place1 + ",\n" +
                        "This " + adj1 + " piggy ate " + noun2 + ",\n" +
                        "This little piggy had " + noun3 + ",\n" +
                        "And THIS little piggy went " + verb1 + " all the way home.";


                OutputStream outputStream = socket.getOutputStream();
                //byte[] bytes = inputStream.readAllBytes();
                //for (int i = 0; i < bytes.length; i++) {
                //    System.out.print(bytes[i]);
                //}

                String htmlResponse = body;

                final String CLRF = "\n\r";

                String response = "HTTP/1.1 200 OK" + CLRF +
                        //"Content-Length: " + htmlResponse.getBytes().length + CLRF + CLRF +
                        htmlResponse + CLRF + CLRF;

                outputStream.write(response.getBytes());

                inputStream.close();
                outputStream.close();
                socket.close();

            } catch (IOException e) {
                System.out.println("Problem with setting socket");
            }
        }


    }
}

package JSON.HTTPServer;

import JSON.HTTPServer.config.Configuration;
import JSON.HTTPServer.config.ConfigurationManager;
import JSON.HTTPServer.core.ServerListenerThread;

import java.io.IOException;


public class HttpServer {



    public static void main(String[] args) {

        ConfigurationManager.getInstance().loadConfigurationFile("src/resources/http.json");

        Configuration conf = ConfigurationManager.getInstance().getCurrentConfiguration();
        System.out.println("Using port: " + conf.getPort());
        System.out.println("Using webRoot: " + conf.getWebroot());

        try {
            ServerListenerThread serverListenerThread = new ServerListenerThread(conf.getPort(), conf.getWebroot());
            serverListenerThread.start();

        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}

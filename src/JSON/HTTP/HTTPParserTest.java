package JSON.HTTP;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class HTTPParserTest {

    private HTTPParser httpParser;

    @BeforeAll
    public void beforeClass(){
        httpParser = new HTTPParser();
    }

    @Test
    void parseHTTPRequests() {
        httpParser.parseHTTPRequests(
                generateValidTestCase()
        );

    }

    private InputStream generateValidTestCase(){
        String rawData = "";

        InputStream inputStream = new ByteArrayInputStream(
                rawData.getBytes(StandardCharsets.US_ASCII)
        );
        return inputStream;
    }
}
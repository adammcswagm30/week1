package JSON.HTTP;

public class HTTPRequest extends HTTPMessage{

    private HTTPMethod method;
    private String requestTarget;
    private String originalHttpVersion; // literal from request
    private HTTPVersion bestCompatibleVersion;

    HTTPRequest() {

    }

    public HTTPMethod getMethod() {
        return method;
    }

    public String getRequestTarget() {
        return requestTarget;
    }

    public String getOriginalHttpVersion() {
        return originalHttpVersion;
    }

    void setMethod(String methodName) throws HTTPParsingException {
        for(HTTPMethod method: HTTPMethod.values()) {
            if (methodName.equals(method.name())) {
                this.method = method;
                return;
            }
            throw new HTTPParsingException(
                    HTTPStatusCode.SERVER_ERROR_501_NOT_IMPLEMENTED
            );
        }
        this.method = HTTPMethod.valueOf(methodName);
    }



    void setRequestTarget(String requestTarget) throws HTTPParsingException {
        if (requestTarget == null || requestTarget.length() == 0){
            throw new HTTPParsingException(HTTPStatusCode.SERVER_ERROR_500_INTERNAL_SERVER_ERROR);
        }
        this.requestTarget =requestTarget;

    }


    public void setHTTPVersion(String originalHttpVersion) throws BadHTTPVersionException, HTTPParsingException {
        this.originalHttpVersion = originalHttpVersion;
        this.bestCompatibleVersion = HTTPVersion.getBestCompatibleVersion(originalHttpVersion);
        if(this.bestCompatibleVersion == null){
            throw new HTTPParsingException(HTTPStatusCode.SERVER_ERROR_505_HTTP_VERSION_NOT_SUPPORTED);
        }
    }
}

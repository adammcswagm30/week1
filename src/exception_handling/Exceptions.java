package exception_handling;

import java.util.Scanner;

public class Exceptions {

    public static void div(int x, int y){
        try{
            float z = x/y;
            System.out.println(x + " divided by " + y + " is: " + z);
        }
        catch (Exception divby0){
            System.out.println("Divide by 0 Error");
        }
        finally {
            System.out.println("Proceeding to exit code");
        }
    }




    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);
        int x = 0;
        int y = 0;
        System.out.println("This program will take two given integers and divides them. \nPlease provide a valid first int:");
        for(int i = 0; i < 1; i ++) {
            if (scanner.hasNextInt()) {
                x = scanner.nextInt();
                System.out.println("Please provide a second int:");
                for(int j = 0; j < 1; j++){
                    if (scanner.hasNextInt()){
                        y = scanner.nextInt();
                        div(x, y);
                    }
                    else{
                        System.out.println("Error, please provide a valid second integer.");
                        scanner.nextLine();
                        scanner.nextLine();
                        j--;
                    }
                }
            } else {
                System.out.println("Error, please provide a valid first integer.");
                scanner.nextLine();
                i--;
            }
        }


    }

}
